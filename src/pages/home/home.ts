import { Component } from '@angular/core';
import { NavController, Platform, AlertController } from 'ionic-angular';
import { Push, PushObject, PushOptions } from '@ionic-native/push';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private push: Push, platform: Platform, public alertCtrl: AlertController) {

    // if(platform.is("cordova")){
      platform.ready().then(() => {
      this.push.hasPermission()
      .then((res: any) => {
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
          this.initpush();
        } else {
          console.log('We do not have permission to send push notifications');
        }

      });
    })
    // }
    
  }

  initpush(){
    const options: PushOptions = {
      android: {},
      ios: {
          alert: 'true',
          badge: true,
          sound: 'false'
      },
      windows: {},
      browser: {
          pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
   };
   
   const pushObject: PushObject = this.push.init(options);
   
   
   pushObject.on('notification').subscribe((notification: any) => {
    console.log('Received a notification', notification);
    const alert = this.alertCtrl.create({
      title: notification.title,
      subTitle: notification.message,
      buttons: ['OK']
    });
    alert.present();
   })

   pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration)); //registrationId
   
   pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }


}
